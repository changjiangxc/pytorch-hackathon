#!/usr/bin/env python3
# Jason format to JSON
# 
# Jason format:
# No. 1
# Allegro I: 0:00
# Adagio II: 2:46
# Menuetto - Allegretto III: 8:14
# Prestissimo: 10:25
# 
# No. 2
# Allegro vivace I: 0:00
# Largo appassionato II: 5:10
# Alegretto III: 12:10
# Graziosos IV: 16:03
# 

import sys

def roman_to_int(s):
    rom_val = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
    int_val = 0
    for i in range(len(s)):
        if i > 0 and rom_val[s[i]] > rom_val[s[i - 1]]:
            int_val += rom_val[s[i]] - 2 * rom_val[s[i - 1]]
        else:
            int_val += rom_val[s[i]]
    return int_val

with open(sys.argv[1], "r") as fh:
    data = fh.read()

chunks = [d.strip() for d in data.split("\n\n")]

all_metadata = {}

for chunk in chunks:
    chunk_sp = chunk.split("\n")
    name = chunk_sp[0]
    #rint("Name: %s" % name)
    for movement_line in chunk_sp[1:]:
        tsplit = [p.strip() for p in movement_line.split(": ")]
        speed_and_movement = tsplit[0]
        timestamp = tsplit[1]
        
        speed_and_movement_sp = speed_and_movement.split(" ")
        speed = " ".join(speed_and_movement_sp[:-1]).lower()
        movement = speed_and_movement_sp[-1]

        #print(movement)
        movement_num = roman_to_int(movement.upper())

        song_metadata = all_metadata.setdefault(name, {})
        section_metadata = song_metadata.setdefault(movement_num, {})

        if section_metadata:
            print("WARNING: duplicate movement number: %d" % movement_num)
            sys.exit(1)
        
        section_metadata["movement"] = movement_num
        section_metadata["timestamp"] = timestamp
        section_metadata["speed"] = speed

templ = """{{"movement": {movement_num}, "start": "{start}", "end": "{end}", "speed": "{speed}"}}"""

for song in all_metadata:
    print("Song: %s" % song)

    song_movements = all_metadata[song]

    start = True
    movement_idx = 0
    while movement_idx < len(song_movements):
        movement_num = movement_idx + 1
        if movement_idx == 0:
            movement_first = song_movements[movement_num]
            movement_second = song_movements[movement_num + 1]
            print(templ.format(movement_num=movement_first["movement"], start="START", end=movement_second["timestamp"], speed=movement_first["speed"]))
        elif movement_idx == len(song_movements) - 1:
            movement_first = song_movements[movement_num]
            print(templ.format(movement_num=movement_first["movement"], start=movement_first["timestamp"], end="END", speed=movement_first["speed"]))
        else:
            # bounded
            movement_first = song_movements[movement_num]
            movement_second = song_movements[movement_num + 1]
            print(templ.format(movement_num=movement_first["movement"], start=movement_first["timestamp"], end=movement_second["timestamp"], speed=movement_first["speed"]))
        movement_idx += 1

