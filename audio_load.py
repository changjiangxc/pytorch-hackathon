import os
import torchaudio
import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np

script_dir = os.path.dirname(os.path.realpath(__file__))
wav_dir = os.path.join(script_dir, "wav")

def get_wav_data_via_torch(wav_filename):
    return torchaudio.load(wav_filename)

def get_wav_data_via_librosa(wav_filename):
    return librosa.load(wav_filename)

data_loader = torch.utils.data.DataLoader(yesno_data,
                                          batch_size=1,
                                          shuffle=True,
                                          num_workers=args.nThreads)

if __name__ == "__main__":
    fn = 'wav/mozart/kunstderfuge/sonatas/no_sections/piano_sonata_279_(hisamori).wav'

    print("Loading via PyTorch...")
    waveform, sample_rate = get_wav_data_via_torch(fn)

    print("Loading via Librosa...")
    waveform2, sample_rate_2 = get_wav_data_via_librosa(fn)

    print("Shape of waveform: {}".format(waveform.size()))
    print("Sample rate of waveform: {}".format(sample_rate))
    print("Type of waveform: {}".format(type(waveform)))

    print("Shape of waveform2: {}".format(waveform2.size))
    print("Sample rate of waveform2: {}".format(sample_rate_2))
    print("Type of waveform2: {}".format(type(waveform2)))

    tempo, beat_frames = librosa.beat.beat_track(y=waveform2, sr=sample_rate_2)
    print('Estimated tempo: {:.2f} beats per minute (Librosa)'.format(tempo))

    beat_times = librosa.frames_to_time(beat_frames, sr=sample_rate_2)
    print("Beat times: %s" % beat_times)

    plt.figure()
    plt.plot(waveform.t().numpy())

    plt.figure()
    plt.plot(waveform2)

    # PyTorch way
    mel_spectrogram_transform = torchaudio.transforms.MelSpectrogram()
    spectrogram_pytorch = mel_spectrogram_transform(waveform)
    print("Shape of spectrogram (PyTorch): {}".format(spectrogram_pytorch.size()))
    plt.figure()
    plt.imshow(spectrogram_pytorch.log2()[0,:,:].detach().numpy(), cmap='gray')

    # Librosa way
    spectrogram_librosa = librosa.feature.melspectrogram(y=waveform2, sr=sample_rate_2)
    print("Shape of spectrogram (Librosa): {}".format(spectrogram_librosa.size))
    plt.figure()
    librosa.display.specshow(librosa.power_to_db(spectrogram_librosa, ref=np.max))

    plt.show()
    input("Press ENTER to exit > ")