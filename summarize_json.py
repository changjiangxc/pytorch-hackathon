#!/usr/bin/env python3
import os
import csv
import json

script_dir = os.path.dirname(os.path.realpath(__file__))
wav_dir = os.path.join(script_dir, "wav")

columns = ["name", "author", "category", "year", "movement", "section", "key", "min_bpm", "max_bpm", "avg_bpm", "med_bpm"]

def summarize_stats(json_file):
    print(json_file)
    resulting_data = []
    with open(json_file, "r") as fh:
        obj = json.loads(fh.read())

        # {
        # 'max_tempo_entry': {'tempo': 413793, 'bpm': 145, 'timestamp_secs': 0.0, 'timestamp_hms': '00:00:00'},
        # 'min_tempo_entry': {'tempo': 3000000, 'bpm': 20, 'timestamp_secs': 1193.6207909083341, 'timestamp_hms': '00:19:53'},
        # 'avg_bpm': 96.9984387197502,
        # 'avg_tempo': 753234.9843871975,
        # 'med_bpm': 96,
        # 'med_tempo': 625000}

        column_data = {
            "author": obj.get("author"),
            "category": obj.get("category"),
            "year": obj.get("year"),
            "key": obj.get("key"),
            "name": obj.get("name"),
            "movement": obj.get("movement")
        }

        if "sections" in obj:
            for section in obj["sections"]:
                new_column_data = dict(**column_data)
                stats = section["tempo_stats"]
                
                new_column_data["section"] = "from_%s_to_%s_speed_%s" % (section.get("start"), section.get("end"), section.get("speed", "unknown"))
                new_column_data["min_bpm"] = stats.get("min_tempo_entry", {}).get("bpm", "unknown")
                new_column_data["max_bpm"] = stats.get("max_tempo_entry", {}).get("bpm", "unknown")
                new_column_data["avg_bpm"] = stats.get("avg_bpm", "unknown")
                new_column_data["med_bpm"] = stats.get("med_bpm", "unknown")
                new_column_data["movement"] = section.get("movement")
                resulting_data.append(new_column_data)
        elif "midi" in obj:
            if "tracks" in obj["midi"]:
                for track, track_data in obj["midi"]["tracks"].items():
                    if "tempo_stats" in track_data and track_data["tempo_stats"]:
                        if "min_bpm" in column_data:
                            print("WARNING: data already exists for %s" % json_file)
                        column_data["section"] = "N/A"
                        column_data["min_bpm"] = track_data["tempo_stats"].get("min_tempo_entry", {}).get("bpm", "unknown")
                        column_data["max_bpm"] = track_data["tempo_stats"].get("max_tempo_entry", {}).get("bpm", "unknown")
                        column_data["avg_bpm"] = track_data["tempo_stats"].get("avg_bpm", "unknown")
                        column_data["med_bpm"] = track_data["tempo_stats"].get("med_bpm", "unknown")
                resulting_data.append(column_data)
            else:
                print("WARNING: no tracks data available for %s" % json_file)
        else:
            print("WARNING: no data available for %s" % json_file)

        #import pdb; pdb.set_trace()
    
    return resulting_data

all_summarized_stats = []

for root, subdirs, json_files in os.walk(wav_dir):
    if json_files:
        for json_file in json_files:
            json_file = os.path.join(root, json_file)
            if json_file.endswith(".json"):
                all_summarized_stats += summarize_stats(json_file)

with open("bpm_stats.csv", "w") as fh:
    writer = csv.DictWriter(fh, fieldnames=columns)
    writer.writeheader()
    for row in all_summarized_stats:
        writer.writerow(row)
