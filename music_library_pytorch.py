#!/usr/bin/env python3
# Music Library... library (for PyTorch)
# Given specific criteria, get the audio files that match said criteria.
# 
# Usage:
#     from music_library import get_audio_files
#     from music_library_pytorch import MusicSelectionDataset
#     
#     # optionally:
#     import torchaudio.transforms as transforms
#     
#     matched_wav_files, matched_music_entries = get_audio_files(criteria...,
#                                                                # optionally:
#                                                                # defaults to True
#                                                                mel_spectrogram=True,
#                                                                # defaults to None
#                                                                transform=transforms.Compose([
#                                                                    transforms.PadTrim(133623,0),
#                                                                    transforms.LC2CL()
#                                                                ]))
#     dataset = MusicSelectionDataset(matched_music_entries)
#     dataloader = torch.utils.data.DataLoader(dataset, batch_size=10,
#                                              shuffle=True, num_workers=10)
# 

import torch
import torchaudio
from torch.utils import data

class MusicLibraryDataset(data.Dataset):
    # PyTorch Dataset for the music library searcher
    # 
    # With some inspiration from:
    # https://github.com/bellchenx/AudioFolder-Dataloader-PyTorch/blob/master/dataloader.py and
    # https://stanford.edu/~shervine/blog/pytorch-how-to-generate-data-parallel
    #
    def __init__(self, music_entries, mel_spectrogram=True, additional_transform=None,
                 chunk_size=1000):
        """Create a MusicLibraryDataset for PyTorch to use.

        This requires you to query the music library first to retrieve a list
        of MusicEntry objects. These objects are needed to create this DataSet.

        Args:
            music_entries (list of MusicEntry): list of music entries to load.
            mel_spectrogram (bool): whether to perform a Mel Spectrogram on the
                data before returning it to the PyTorch DataLoader. Defaults to
                True.
            additional_transform (func, optional): additional transformation
                functions/objects to apply to the waveform before returning it
                to the PyTorch DataLoader.
        """
        self.music_entries = music_entries
        self.mel_spectrogram = mel_spectrogram
        self.additional_transform = additional_transform

        self.chunk_size = chunk_size

        self.chunk_idx = []

        # Preload sizes
        print("preprocessing %d music_entries into slices..." % len(self.music_entries))
        ctr = 0
        remainder_wav_len = 0
        for music_entry in list(self.music_entries)[:10]:
            ctr += 1
            #if ctr % 30 == 0:
            #    print("processed %d music_entries into slices..." % ctr)
            print("preprocessing %d: %s" % (ctr, music_entry.wav_file))

            with torch.no_grad():
                audio_waveform, sample_rate = torchaudio.load(music_entry.wav_file)

                if audio_waveform.shape[1] < 100:
                    print("skipping due to small size of %d: %s" % (audio_waveform.shape[1], music_entry.wav_file))
                    continue

                if self.mel_spectrogram:
                    mel_spectrogram_transform = torchaudio.transforms.MelSpectrogram()
                    audio_waveform = mel_spectrogram_transform(audio_waveform)
                    total_wav_len = audio_waveform.shape[2]
                else:
                    total_wav_len = audio_waveform.shape[1]

                if self.additional_transform:
                    audio_waveform = self.additional_transform(audio_waveform)
        
                cur_wav_pos = 0

                while cur_wav_pos < total_wav_len:
                    if remainder_wav_len:
                        sl = slice(cur_wav_pos, cur_wav_pos + remainder_wav_len)
                        remainder_wav_len = 0
                    else:
                        sl = slice(cur_wav_pos, cur_wav_pos + self.chunk_size)
                    
                    cur_wav_pos += self.chunk_size
                    self.chunk_idx.append({
                        "music_entry": music_entry,
                        "slice": sl
                    })

                remainder_wav_len = cur_wav_pos - total_wav_len

    def __len__(self):
        # Defines total # of samples
        #return len(self.music_entries)
        return len(self.chunk_idx)

    def __getitem__(self, index):
        print("Index retrieved: " + str(index))

        # This generates one sample of data, based on index.
        # Select sample:
        #music_entry = list(self.music_entries)[index]
        chunk = self.chunk_idx[index]

        music_entry = chunk["music_entry"]
        sl = chunk["slice"]

        print("providing: %s slice %s" % (music_entry.wav_file, sl))

        # time domain
        # frequency domension

        with torch.no_grad():
            audio_waveform, sample_rate = torchaudio.load(music_entry.wav_file)

            print("input audio shape: %s" % str(audio_waveform.shape))

            if self.mel_spectrogram:
                mel_spectrogram_transform = torchaudio.transforms.MelSpectrogram(sample_rate)
                audio_waveform = mel_spectrogram_transform(audio_waveform)
                print("mel audio_waveform shape pre-slice: %s" % (str(audio_waveform.shape)))
                audio_waveform = audio_waveform[:, :, sl]
                print("mel audio_waveform shape post-slice: %s" % (str(audio_waveform.shape)))
                audio_waveform = audio_waveform[0, :, :]
                #if audio_waveform.shape[2] < self.chunk_size:
                #    audio_waveform = torch.reshape(audio_waveform, (audio_waveform.shape[0], audio_waveform.shape[1], 1000))
                #    print("mel audio_waveform shape post-fill: %s" % (str(audio_waveform.shape)))
            else:
                print("audio_waveform pre shape: %s (%s)" % (str(audio_waveform.shape), str(sl)))
                audio_waveform = audio_waveform[:, sl]
                print("audio_waveform post shape: %s" % str(audio_waveform.shape))
            if audio_waveform.shape[1] < self.chunk_size:
                audio_waveform = torch.reshape(audio_waveform, (audio_waveform.shape[0], 1000))

            if self.additional_transform:
                audio_waveform = self.additional_transform(audio_waveform)

        print(type(audio_waveform))

        # Load data and get label
        label = music_entry.wav_file

        print("returning dataset: %s" % str(audio_waveform.shape))

        #import pdb; pdb.set_trace()

        #print(audio_waveform[0, :, :].shape)

        return audio_waveform, label
