from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .forms import SearchForm

from music_library import get_audio_files

# Create your views here.
def index(request):
    template = loader.get_template('data_explorer/home.html')

    if request.method == 'POST':
        form = SearchForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.author
            return HttpResponse(template.render({"form": form, "results": "test"}, request))

    return HttpResponse(template.render({"form": SearchForm}, request))
