from __future__ import print_function
#%matplotlib inline
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# Audio specific imports
from music_library import get_audio_files
from music_library_pytorch import MusicLibraryDataset

import torchaudio.transforms as transforms

# 
# Idea for tomorrow... this seems to have spurred based on
# us changing channels from 1 to 128... maybe setting that to 1
# would do something? Probably not...
# 

# Number of workers for dataloader
workers = 2
batch_size = 4

# image_size = 64

# Number of channels in the training images. For color images this is 3
# Audio assumption is single channel
nc = 128
# Size of z latent vector (i.e. size of generator input)
nz = 1
# Size of feature maps in generator
ngf = 64
# Size of feature maps in discriminator
ndf = 64
# Number of training epochs
num_epochs = 5
# Learning rate for optimizers
lr = 0.0002
# Beta1 hyperparam for Adam optimizers
beta1 = 0.5
# Number of GPUs available. Use 0 for CPU mode.
ngpu = 1
# Kernel size
kernel_size = 1

# Load audio dataset
matched_wav_files, matched_music_entries = get_audio_files()
dataset = MusicLibraryDataset(matched_music_entries, mel_spectrogram=True)

# Create the dataloader
dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size,
                                         shuffle=True, num_workers=workers)

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)

# Decide which device we want to run on
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")


class Generator(nn.Module):
    def __init__(self, ngpu):
        super(Generator, self).__init__()
        self.ngpu = ngpu
        self.main = nn.Sequential(
            # input is Z, going into a convolution
            # nn.ConvTranspose1d(nz, ngf * 256, kernel_size, 1, 0, bias=False),
            # nn.BatchNorm1d(ngf * 256),
            # nn.ReLU(True),

            nn.ConvTranspose1d(nz, ngf * 128, kernel_size, 1, 0, bias=False),
            nn.BatchNorm1d(ngf * 128),
            nn.ReLU(True),

            nn.ConvTranspose1d(ngf * 128, ngf * 64, kernel_size, 1, 0, bias=False),
            nn.BatchNorm1d(ngf * 64),
            nn.ReLU(True),

            nn.ConvTranspose1d(ngf * 64, ngf * 32, kernel_size, 1, 0, bias=False),
            nn.BatchNorm1d(ngf * 32),
            nn.ReLU(True),

            nn.ConvTranspose1d(ngf * 32, ngf * 16, kernel_size, 1, 0, bias=False),
            nn.BatchNorm1d(ngf * 16),
            nn.ReLU(True),

            nn.ConvTranspose1d(ngf * 16, ngf * 8, kernel_size, 1, 0, bias=False),
            nn.BatchNorm1d(ngf * 8),
            nn.ReLU(True),

            nn.ConvTranspose1d(ngf * 8, ngf * 4, kernel_size, 1, 0, bias=False),
            nn.BatchNorm1d(ngf * 4),
            nn.ReLU(True),

            nn.ConvTranspose1d(ngf * 4, ngf * 2, kernel_size, 1, 0, bias=False),
            nn.BatchNorm1d(ngf * 2),
            nn.ReLU(True),

            nn.ConvTranspose1d(ngf * 2, ngf, kernel_size, 1, 0, bias=False),
            nn.BatchNorm1d(ngf),
            nn.ReLU(True),

            nn.ConvTranspose1d(ngf, nc, kernel_size, 2, 1, bias=False),
            nn.Tanh()
        )

    def forward(self, input):
        return self.main(input)

netG = Generator(ngpu).to(device)

# Handle multi-gpu if desired
if (device.type == 'cuda') and (ngpu > 1):
    netG = nn.DataParallel(netG, list(range(ngpu)))

# Apply the weights_init function to randomly initialize all weights
#  to mean=0, stdev=0.2.
netG.apply(weights_init)

# Print the model
print(netG)


class Discriminator(nn.Module):
    def __init__(self, ngpu):
        super(Discriminator, self).__init__()
        self.ngpu = ngpu
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64

            nn.Conv1d(nc, ndf, kernel_size, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv1d(ndf, ndf * 2, kernel_size, 2, 1, bias=False),
            nn.BatchNorm1d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv1d(ndf * 2, ndf * 4, kernel_size, 2, 1, bias=False),
            nn.BatchNorm1d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv1d(ndf * 4, ndf * 8, kernel_size, 2, 1, bias=False),
            nn.BatchNorm1d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv1d(ndf * 8, ndf * 16, kernel_size, 2, 1, bias=False),
            nn.BatchNorm1d(ndf * 16),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv1d(ndf * 16, ndf * 32, kernel_size, 2, 1, bias=False),
            nn.BatchNorm1d(ndf * 32),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv1d(ndf * 32, ndf * 64, kernel_size, 2, 1, bias=False),
            nn.BatchNorm1d(ndf * 64),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv1d(ndf * 64, ndf * 128, kernel_size, 2, 1, bias=False),
            nn.BatchNorm1d(ndf * 128),
            nn.LeakyReLU(0.2, inplace=True),

            # nn.Conv1d(ndf * 128, ndf * 256, kernel_size, 2, 1, bias=False),
            # nn.BatchNorm1d(ndf * 256),
            # nn.LeakyReLU(0.2, inplace=True),

            nn.Conv1d(ndf * 256, 1, kernel_size, 1, 0, bias=False),
            nn.Sigmoid()
        )

    def forward(self, input):
        return self.main(input)


# Create the Discriminator
netD = Discriminator(ngpu).to(device)

# Handle multi-gpu if desired
if (device.type == 'cuda') and (ngpu > 1):
    netD = nn.DataParallel(netD, list(range(ngpu)))

# Apply the weights_init function to randomly initialize all weights
#  to mean=0, stdev=0.2.
netD.apply(weights_init)

# Print the model
print(netD)

criterion = nn.BCELoss()

# Create batch of latent vectors that we will use to visualize
#  the progression of the generator

# fixed_noise = torch.randn(64, nz, 1, 1, device=device)
fixed_noise = torch.randn(64, nz, 1)


# Establish convention for real and fake labels during training
real_label = 1
fake_label = 0

# Setup Adam optimizers for both G and D
optimizerD = optim.Adam(netD.parameters(), lr=lr, betas=(beta1, 0.999))
optimizerG = optim.Adam(netG.parameters(), lr=lr, betas=(beta1, 0.999))

# img_list = []
wav_list = []

G_losses = []
D_losses = []
iters = 0

print("Starting Training Loop...")
# For each epoch
for epoch in range(num_epochs):
    # For each batch in the dataloader
    print(" **** epoch run: %d" % epoch)
    for i, data in enumerate(dataloader, 0):
        print(" **** data loading now...")
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        ## Train with all-real batch
        print(" **** update zgrad for netD...")
        #netD.zero_grad()
        with torch.no_grad():
            # Format batch
            print(" **** data=%s" % str(data))
            real_cpu = data[0].to(device)
            print(" **** real_cpu=%s" % str(real_cpu))
            print(" **** real_cpu size()=%s" % str(real_cpu.size()))
            b_size = real_cpu.size(0)
            print(" **** b_size=%d" % b_size)
            label = torch.full((b_size,), real_label, device=device)
            # Forward pass real batch through D
            print(" **** netD real_cpu")
            output = netD(real_cpu).view(-1)
            # Calculate loss on all-real batch
            print(" **** loss calc")
            print(" **** output = %s" % str(output))
            print(" **** label = %s" % str(label))
            #errD_real = criterion(output, label)
            # Calculate gradients for D in backward pass
            print(" **** grad calc")
            #errD_real.backward()
            D_x = output.mean().item()

            print(" **** update 1/2 done...")

            ## Train with all-fake batch
            # Generate batch of latent vectors
            print(" **** randn...")
            # noise = torch.randn(nz, b_size, 1, device=device)
            # noise = torch.randn(b_size, nz, 1)
            # noise = torch.randn(64, 1, 1)
            noise = torch.randn(ngf, nz, 1, device=device)
            # Generate fake batch with G
            print(" **** netG...")
            
            # sel_index = random.randint(0,len(dataset))
            # something_real = dataset[sel_index]
            
            # fake = netG(something_real)
            fake = netG(fixed_noise)
            
            print(" **** fake label fill...")
            label.fill_(fake_label)
            # Classify all fake batch with D
            print(" **** classify fake batch w/D...")
            output = netD(fake.detach()).view(-1)
            # Calculate D's loss on the all-fake batch
            #errD_fake = criterion(output, label)
            # Calculate the gradients for this batch
            #errD_fake.backward()
            print(" **** mean...")
            D_G_z1 = output.mean().item()
            # Add the gradients from the all-real and all-fake batches
            #errD = errD_real + errD_fake
            # Update D
            print(" **** update D...")
            optimizerD.step()
            print(" **** update 1 done...")

        ############################
        # (2) Update G network: maximize log(D(G(z)))
        ###########################
        #netG.zero_grad()
        with torch.no_grad():
            label.fill_(real_label)  # fake labels are real for generator cost
            # Since we just updated D, perform another forward pass of all-fake batch through D
            output = netD(fake).view(-1)
            # Calculate G's loss based on this output
            #errG = criterion(output, label)
            # Calculate gradients for G
            #errG.backward()
            D_G_z2 = output.mean().item()
            # Update G
            optimizerG.step()

            # Output training stats
            #if i % 50 == 0:
            #    print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
            #        % (epoch, num_epochs, i, len(dataloader),
            #            errD.item(), errG.item(), D_x, D_G_z1, D_G_z2))

            print(" **** update 2 done...")

            # Save Losses for plotting later
            #G_losses.append(errG.item())
            #D_losses.append(errD.item())

        # Check how the generator is doing by saving G's output on fixed_noise
        if (iters % 500 == 0) or ((epoch == num_epochs - 1) and (i == len(dataloader) - 1)):
            with torch.no_grad():
                fake = netG(fixed_noise).detach().cpu()
            torchaudio.save("epoch_%d.wav" % epoch, fake, 44100)
            wav_list.append("epoch_%d.wav" % epoch)

        print(" **** iter done...")
        iters += 1

#plt.figure(figsize=(10,5))
#plt.title("Generator and Discriminator Loss During Training")
#plt.plot(G_losses,label="G")
#plt.plot(D_losses,label="D")
#plt.xlabel("iterations")
#plt.ylabel("Loss")
#plt.legend()
#plt.show()
